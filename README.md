**#Udemy-frontend course PJ01**
URL: https://yh-hawaii.netlify.app/
<br>
A front end practicing project of the Udemy course. Coding with HTML5 and CSS.
這是第二個跟著線上學習課程做的前端網頁，在做這個網頁的過程中釐清了section與div切版的概念，也更了解如何使用justify-content
遇到比較大的困難是自己在做footer時，因為body的height設定用絕對值，因此footer會蓋到body資訊，找了很久最後才在老師的教學影片裡看到這個錯誤。存檔時也因為少了一個closing tag而一直無法存檔，排版練習上需要再更注意。
<br>
**#Special Thanks**
<br>
Undemy course [2022網頁開發全攻略(HTML, CSS, JavaScript, React, SQL, Node, more)](https://www.udemy.com/course/html5-css3-z/)
